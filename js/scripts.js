// this function constructs the figure file name based on the selected
// dropdown menu options, then uses this to update the figure field
function loadFigure() {
	// regime name from the regime dropdown
	var regime = document.getElementById("regime").value;
	// parameter name from the parameter dropdown
	var param = document.getElementById("param").value;
	// frequency of the data from the frequency dropdown
	var freq = document.getElementById("freq").value;
	// and rain days only from the rain days only dropdown
	var rdo = document.getElementById("rdo").value;
	// put all together to form path to figure
	var figurepath = 
		"figures/" + 
		[regime, freq, param, rdo].join('_') + 
		".png"
	document.getElementById("figure").src = figurepath
	if (freq == "3-hourly") {
		document.getElementById("figure").style.width = "84%";
	} else if (freq == "Daily") {
		document.getElementById("figure").style.width = "45%";
	}
}

// this function builds the title text for the figure, based
// on the selected dropdown menu options
function makeTitle() {
	// make a dictionary for prettyfying the regime names
	var regime_dict = {
		"October": "October",
		"November": "November",
		"December": "December",
		"January": "January",
		"February": "February",
		"March": "March",
		"April": "April",
		"Wet-season-all": "Wet Season - all",
		"Build-up-all": "Build-Up - all",
		"Early-build-up": "Early Build-Up",
		"Late-build-up": "Late Build-Up",
		"Active-monsoon": "Active Monsoon",
		"Monsoon-break-all": "Monsoon Break - all",
		"Easterly-monsoon-break": "Easterly Break",
		"Westerly-monsoon-break": "Westerly Break"
	}
	// dictionary for prettyfying the rain days only text
	var rdo_dict = {
		"False": "(all days)",
		"True": "(rain days only)"
	}
	// this is the figure's title
	var title = 
		regime_dict[document.getElementById("regime").value] + ", " +
		document.getElementById("freq").value + " " +
		document.getElementById("param").value + " " +
		rdo_dict[document.getElementById("rdo").value]
	// set the title in the html
	document.getElementById("figure_title").innerHTML = title
}

// function to enable toggleable about section
function toggleContent() {
	var about_title = document.getElementById("about_title").innerHTML;
	// if about section is hidden, then show and change triangle symbol
	if (about_title == String.fromCharCode(9656) + " About") {
		document.getElementById("about_title").innerHTML = "&#9662 About";
		document.getElementById("about_content").style.visibility = "visible";
	// if about section is shown then unshow and change triangle symbol
	} else if (about_title == String.fromCharCode(9662) + " About") {
		document.getElementById("about_title").innerHTML = "&#9656 About";
		document.getElementById("about_content").style.visibility = "hidden";
	}
}